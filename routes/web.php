<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticlesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
});
// Route::get('/articles', [ArticlesController::class, 'index']);
Route::get('/articles', 'ArticlesController@index')->name('articles.index');
Route::get('/articles/create', 'ArticlesController@create')->name('articles.create');
Route::post('articles/', 'ArticlesController@store')->name('articles.store');
Route::get('articles/{article}', 'ArticlesController@show')->name('articles.show');
Route::get('articles/{article}/edit', 'ArticlesController@edit')->name('articles.edit');
Route::put('/articles/{article}', 'ArticlesController@update')->name('articles.update');

/**
 * GET, POST, PUT, PATCH, DELETE
 *
 * GET: /articles - "To load all articles"
 * GET: /articles/:id - "To load an article having id as id"
 * DELETE: /articles/:id/delete - "To delete an article having id as 'id'
 * GET: /articles/:id/update - displays the view of an article having id as 'id'
 * PUT: /articles/:id - To persist the changes in 'id'
 * POST: /articles - To persist new article
 * GET: /articles/create - To show the view to create an article
 */
