<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * This method is to be overwritten when we need to change the Route Model Binding Column
     * By default the column is 'id' which is specified in the base Model class
    */
    // public function getRouteKeyName() {
    //     return 'title';
    // }

    protected $fillable = ['title', 'excerpt', 'body']; //Sirf yehi sab hi fillable hai

    // protected $guarded = ['created_at', 'updated_at']; //Isko chhodke baaki sab fillable hai.

    public function tags(){
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
}
