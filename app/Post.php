<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function author(){
        return $this->belongsTo(User::class, 'user_id'); //user ke paas author_id nhi hai toh woh null dega isliye hume specify krna pada user_id lene. By default method name is author so it would search for author_id.
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}
