<?php

namespace App\Http\Controllers;
use App\Article;
use App\Tag;

use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index(){
        $tags = Tag::all();
        // $articles = Article::all();
        // $articles = Article::take(2)->get();
        // $articles = Article::paginate(2);
        // $articles = Article::latest()->get();
        // $articles = Article::latest('updated_at')->get();
        // $articles = Article::take(2)->latest('updated_at')->get();
        // return $articles;
        if(request('tag')){
            $articles = Tag::where('name', request('tag'))->firstOrFail()->articles()->paginate(4);
        }else{
            $articles = Article::latest('updated_at')->paginate(4);
        }
        return view('articles.index', [
            'articles' => $articles,
            'tags' => $tags
        ]);
    }
    public function show(Article $article){
        $tags = Tag::all();
        return view('articles.show', [
            'article'=>$article,
            'tags' => $tags
        ]);
    }
    public function create(){
        $tags = Tag::all();
        return view('articles.create' , compact(['tags']));
    }
    public function edit(Article $article){
        return view('articles.edit', [
            'article' => $article
        ]);
    }
    public function store(){
        //GET or POST => request => Request - request()
        // dd(request()->all());
        // dd(request('title'));
        // dd(request()->input('title'));
        // $validatedAttributes = request()->validate([
            // 'title' => 'required',
            // 'excerpt' => 'required',
            // 'body' => 'required'
        // ]); //will return to edit page if validator failed!
        /*$article = new Article();
        $article->title = request()->input('title');
        $article->excerpt = request()->input('excerpt');
        $article->body = request()->input('body');
        $article->save();*/

        // Article::create([
        //     'title' => request('title'),
        //     'excerpt' => request('excerpt'),
        //     'body' => request('body')
        // ]);
        // Article::create(request()->all());
        // Article::create($validatedAttributes);
        $article = new Article($this->validateData());
        $article->user_id = 1;
        $article->save();
        $article->tags()->attach(request('tags'));
        return redirect(route('articles.index'));
    }
    public function update(Article $article){
        // $article->title = request()->input('title');
        // $article->excerpt = request()->input('excerpt');
        // $article->body = request()->input('body');
        // $article->save();
        $article->update($this->validateData());
        return redirect(route('articles.show', [$article->id]));
    }
    public function validateData() {
        return request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
            'tags' => 'exists:tags,id'
        ]);
    }
}
