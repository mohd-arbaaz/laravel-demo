@extends('layouts.app')

@section('title','Blog')

@section('main-content')
<h1 class="my-4">Page Heading
    <small>Secondary Text</small>
  </h1>

  <!-- Blog Post -->
  @forelse ($articles as $article)

  <div class="card mb-4">
      <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
      <div class="card-body">
          <h2 class="card-title">{{$article->title}}</h2>
          <p class="card-text">{{$article->excerpt}}</p>
        <a href="{{route('articles.show', [$article->id])}}" class="btn btn-primary">Read More &rarr;</a>
        </div>
        <div class="card-footer text-muted d-flex justify-content-between">
            <p> Posted on January 1, 2017 by <a href="#">Start Bootstrap</a></p>
            <p>
                @foreach ($article->tags as $tag)
                    <a href="/articles?tag={{$tag->name}}">{{ $tag->name }}</a>
                @endforeach
            </p>
        </div>
    </div>
    @empty
        <p>No Articles to Display</p>
    @endforelse

  <!-- Pagination -->
  {{$articles->links()}}
@endsection
