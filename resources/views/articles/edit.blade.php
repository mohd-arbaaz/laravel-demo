@extends('layouts.app')

@section('main-content')
    <h1 class="mt-4">Edit Article</h1>
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <form action="{{route('articles.update', [$article->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control {{$errors->has('title') ? 'is-invalid' : '' }}"
                id="title" name="title" value="{{ old('title', $article->title)}}">
            @if ($errors->has('title'))
                <p class="text-danger font-weight-light">{{ $errors->first('title') }}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="excerpt">Excerpt</label>
            <input type="text" class="form-control @error('excerpt') is-invalid @enderror" id="excerpt" name="excerpt"  value="{{ old('excerpt', $article->excerpt)}}">
            @error('excerpt')
                <p class="text-danger font-weight-light">{{ $errors->first('excerpt') }}</p>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea name="body" id="body" rows="5" class="form-control @error('body') is-invalid @enderror">
                {{ old('body', $article->body) }}
            </textarea>
            @error('body')
            <p class="text-danger font-weight-light">{{ $message }}</p>
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-outline-primary">
        </div>
    </form>
@endsection
